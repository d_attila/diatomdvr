"""A script for running the doctests of diatomdvr.
"""


import os
import sys
os.chdir(os.path.dirname(__file__) + "/diatomdvr")
sys.path.append(os.path.dirname(__file__))
import doctest
import diatomdvr.dvr as dvr
import diatomdvr.curve as curve
import diatomdvr.morse_to_polynomial as morse_to_polynomial


doctest.testmod(dvr)
print("Doctest of diatomdvr.dvr...")
doctest.testmod(curve)
print("Doctest of diatomdvr.curve...")
doctest.testmod(morse_to_polynomial)
print("Doctest of diatomdvr.morse_to_polynomial...")
input()
