"""Calculating the rotational-vibrational energy states of a diatomic
molecule using discrete variable representation (DVR) method.
"""


import numpy as np
import molgemtools.constants as constants
import diatomdvr.curve as curve


class DVR:
    r"""Methods for performing DVR calculation of ro-vibrational
    energy states of diatomic molecules.

    Parameters
    ----------
    n : int
        The number of grid points.
    r_min : int or float
        Minimum bond length in Bohr units.
    r_max : int or float
        Maximum bond length in Bohr units.
    atoms : array_like of str
        A list or tuple of the chemical symbols of the atoms of the
        diatomic molecule.
    c_vec : array_like of int or float
        A vector of the coefficients of the fitting polynomial of
        Morse variables.
    a : int or float, optional
        A constant parameter in the exponential terms of the fitting
        polynomial.
    j_rot : int, optional
        The rotational quantum number.

    Notes
    -----
    Let :math:`\textbf{Q}` be the following tridiagonal matrix with
    zero diagonal entries:

    .. math::
        \textbf{Q}_{i,i+1}=\textbf{Q}_{i+1,i}=\sqrt{\frac{i+1}{2}}.

    The eigenvalue decomposition of :math:`\textbf{Q}`:

    .. math::
        \textbf{Q}=\textbf{T}diag(\textbf{q})\textbf{T}^T,

    where :math:`\textbf{T}` is a transformation matrix can be used to
    convert the FBR kinetic energy matrix to DVR. The eigenvalues of
    :math:`\textbf{Q}` are can be converted to the vector of grid
    points, :math:`\textbf{r}`.

    .. math::
        \textbf{r}_i
        =\frac{\textbf{q}_i}{x}
        +\frac{\textbf{r}_{max}-\textbf{r}_{min}}{2}

        x=\frac{2\textbf{q}_{max}}{\textbf{r}_{max}-\textbf{r}_{min}}

    Examples
    --------
    >>> import diatomdvr.dvr as dvr
    >>> c = [0.16969615, -3.66343563, 19.77175143]
    >>> a = 1.012540518
    >>> HCl = dvr.DVR(5, 1.7, 10, ['H', 'Cl'], c, a=a)
    >>> q_mat = HCl.q_mat
    >>> print(q_mat.round(4))
    [[0.     0.7071 0.     0.     0.    ]
     [0.7071 0.     1.     0.     0.    ]
     [0.     1.     0.     1.2247 0.    ]
     [0.     0.     1.2247 0.     1.4142]
     [0.     0.     0.     1.4142 0.    ]]
    >>> t_mat = HCl.t_mat
    >>> print(t_mat.round(4))
    [[ 0.1061  0.4712  0.7303 -0.4712  0.1061]
     [-0.3031 -0.6388 -0.     -0.6388  0.3031]
     [ 0.5373  0.2791 -0.5164 -0.2791  0.5373]
     [-0.6388  0.3031  0.      0.3031  0.6388]
     [ 0.4472 -0.4472  0.4472  0.4472  0.4472]]
    >>> q_vec = HCl.q_vec
    >>> for q in q_vec:
    ...     print(round(q, 4))
    -2.0202
    -0.9586
    -0.0
    0.9586
    2.0202
    >>> r_vec = HCl.r_vec
    >>> for r in r_vec:
    ...     print(round(r, 4))
    1.7
    3.8808
    5.85
    7.8192
    10.0
    >>> round(HCl.x, 4)
    0.4868
    >>> round(2*q_vec[-1]/(r_vec[-1] - r_vec[0]), 4)
    0.4868
    """
    # Atomic unit of mass in kg.
    u = constants.Constants.u
    # Planck constant in J*s.
    h = constants.Constants.h
    # The speed of light in vacuum in m/s.
    c = constants.Constants.c
    # Hartree energy in J.
    e_h = constants.Constants.e_h
    # Electron mass in kg.
    m_e = constants.Constants.m_e
    # A dictionary of atomic masses.
    m_dict = constants.Constants.m_dict

    def __init__(self, n, r_min, r_max, atoms, c_vec, a=2, j_rot=0):
        self.n = n
        if not isinstance(self.n, int):
            raise TypeError("The number of grid points should be an int.")
        if self.n < 1:
            raise ValueError("The number of grid points should be positive.")
        self.r_min = r_min
        self.r_max = r_max
        if self.r_min <= 0 or self.r_max <= 0:
            raise ValueError("A bond length should be positive.")
        if self.r_min >= self.r_max:
            raise ValueError("r_max should be bigger than r_min.")
        self.atoms = atoms
        if len(self.atoms) != 2:
            raise ValueError("Expected two chemical symbols.")
        self.c_vec = np.array(c_vec)
        self.a = a
        if self.a <= 0:
            raise ValueError("Parameter a should be positive.")
        self.j_rot = j_rot
        if not isinstance(self.j_rot, int):
            raise TypeError("j_rot should be an int.")
        if self.j_rot < 0:
            raise ValueError("j_rot should be non-negative.")
        # The reciprocal of the reduced mass in electron mass units.
        self.mu_rec = sum([1/(DVR.m_dict[atom]*DVR.u/DVR.m_e)
                           for atom in self.atoms])
        # Q matrix.
        self.q_mat = np.zeros((self.n, self.n))
        for i in range(self.n - 1):
            self.q_mat[i][i + 1] = ((i + 1)/2)**0.5
            self.q_mat[i + 1][i] = self.q_mat[i][i + 1]
        # Eigenvalue decomposition of Q matrix.
        q_eigval, q_eigvec = np.linalg.eig(self.q_mat)
        # Sorting the eigenvalues and eigenvectors of Q.
        q_eig_zip = zip(q_eigval, q_eigvec.T)
        q_eig_sorted = sorted(q_eig_zip, key=lambda q: q[0])
        # The sorted eigenvalues of Q matrix.
        self.q_vec = np.array([q[0] for q in q_eig_sorted])
        # Transformation matrix, the eigenvectors of Q.
        self.t_mat = np.array([q[1] for q in q_eig_sorted]).T
        self.x = 2*self.q_vec[-1]/(self.r_max - self.r_min)
        # Vector of grid points.
        self.r_vec = np.array([q/self.x + 0.5*(self.r_min + self.r_max)
                               for q in self.q_vec])

    def potential_energy_matrix(self):
        r"""Calculates the potential energy matrix.

        Returns
        -------
        v_mat : ndarray

        Notes
        -----
        Let :math:`\textbf{r}` be a vector representing bond lengths
        at the grid points. The corresponding elements of the diagonal
        potential energy matrix are can be defined as a function of
        Morse variables:

        .. math::
            \textbf{V}_{i,i}(\textbf{r}_i)
            =\sum\limits_{n=0}^{k}c_ne^{-n\frac{\textbf{r}_i}{a}},

        where :math:`k` denotes the degree of the fitted polynomial
        and the :math:`c_n`-s are linear coefficients.

        Examples
        --------
        >>> import diatomdvr.dvr as dvr
        >>> c = [0.16969615, -3.66343563, 19.77175143]
        >>> a = 1.012540518
        >>> HCl = dvr.DVR(5, 1.7, 10, ['H', 'Cl'], c, a=a)
        >>> v_mat = HCl.potential_energy_matrix()
        >>> print(v_mat.round(4))
        [[0.1744 0.     0.     0.     0.    ]
         [0.     0.0997 0.     0.     0.    ]
         [0.     0.     0.1585 0.     0.    ]
         [0.     0.     0.     0.1681 0.    ]
         [0.     0.     0.     0.     0.1695]]
        """
        v_mat = np.diag([curve.potential_energy(r, self.c_vec, a=self.a)
                         for r in self.r_vec])
        return v_mat

    def rotational_energy_matrix(self):
        r"""Calculates the rotational energy matrix.

        Returns
        -------
        j_mat : ndarray

        Notes
        -----
        The non-zero elements of the diagonal rotational energy matrix
        are can be defined as:

        .. math::
            \textbf{J}_{i,i}(\textbf{r}_i)
            =\frac{1}{2}\frac{J(J+1)}{\mu \textbf{r}_i^2},

        where :math:`J` denotes the rotational quantum number and
        :math:`\mu` is the reduced mass of the diatomic molecule.

        Examples
        --------
        >>> import diatomdvr.dvr as dvr
        >>> c = [0.16969615, -3.66343563, 19.77175143]
        >>> a = 1.012540518
        >>> HCl = dvr.DVR(5, 1.7, 10, ['H', 'Cl'], c, a=a, j_rot=20)
        >>> j_mat = HCl.rotational_energy_matrix()
        >>> print(j_mat.round(4))
        [[0.0407 0.     0.     0.     0.    ]
         [0.     0.0078 0.     0.     0.    ]
         [0.     0.     0.0034 0.     0.    ]
         [0.     0.     0.     0.0019 0.    ]
         [0.     0.     0.     0.     0.0012]]
        """
        j_mat = np.diag([0.5*self.mu_rec*self.j_rot*(self.j_rot + 1)/r**2
                         for r in self.r_vec])
        return j_mat

    def kinetic_energy_matrix(self):
        r"""Calculates the DVR kinetic energy matrix.

        Returns
        -------
        k_mat : ndarray

        Notes
        -----
        The FBR kinetic energy matrix is a sparse matrix with the
        following non-zero elements:

        .. math::
            \textbf{K}_{i,i}^{FBR}=x^2\left(\frac{1}{2}-i\right)

            \textbf{K}_{i,i+2}^{FBR}=\textbf{K}_{i+2,i}^{FBR}
            =x^2\frac{1}{2}\sqrt{i(i+1)},

        where :math:`x=\frac{2\textbf{q}_{max}}{\textbf{r}_{max}
        -\textbf{r}_{min}}`. The :math:`\textbf{K}^{FBR}` can be
        converted to the DVR kinetic energy matrix,
        :math:`\textbf{K}`.

        .. math::
            \textbf{K}
            =-\frac{1}{2\mu}\textbf{T}^T\textbf{K}^{FBR}\textbf{T}

        Examples
        --------
        >>> import numpy as np
        >>> import diatomdvr.dvr as dvr
        >>> np.set_printoptions(suppress=True)
        >>> c = [0.16969615, -3.66343563, 19.77175143]
        >>> a = 1.012540518
        >>> HCl = dvr.DVR(5, 1.7, 10, ['H', 'Cl'], c, a=a)
        >>> k_mat = HCl.kinetic_energy_matrix()
        >>> print(k_mat.round(6))
        [[ 0.000088 -0.000064  0.000015 -0.000017 -0.000017]
         [-0.000064  0.000114 -0.000101 -0.00002   0.000017]
         [ 0.000015 -0.000101  0.000093  0.000101  0.000015]
         [-0.000017 -0.00002   0.000101  0.000114  0.000064]
         [-0.000017  0.000017  0.000015  0.000064  0.000088]]
        """
        # FBR kinetic energy matrix.
        k_fbr_mat = self.x**2*np.diag([0.5 - i for i in range(self.n)])
        for i in range(self.n - 2):
            k_fbr_mat[i][i + 2] = self.x**2*0.5*(i*(i + 1))**0.5
            k_fbr_mat[i + 2][i] = k_fbr_mat[i][i + 2]
        # Transforming the FBR kinetic energy matrix into DVR.
        k_dvr_mat = self.t_mat.T @ k_fbr_mat @ self.t_mat
        k_mat = -0.5*self.mu_rec*k_dvr_mat
        return k_mat

    def hamiltonian_matrix(self):
        r"""Calculates the DVR Hamiltonian matrix.

        Returns
        -------
        h_mat : ndarray

        Notes
        -----
        The Hamiltonian matrix is the sum of the kinetic energy,
        rotational energy and the potential energy matrices. It is a
        symmetric and real matrix.

        .. math::
            \textbf{H}=\textbf{K}+\textbf{J}+\textbf{V}.

        Examples
        --------
        >>> import numpy as np
        >>> import diatomdvr.dvr as dvr
        >>> np.set_printoptions(suppress=True)
        >>> c = [0.16969615, -3.66343563, 19.77175143]
        >>> a = 1.012540518
        >>> HCl = dvr.DVR(5, 1.7, 10, ['H', 'Cl'], c, a=a)
        >>> h_mat = HCl.hamiltonian_matrix()
        >>> print(h_mat.round(6))
        [[ 0.174522 -0.000064  0.000015 -0.000017 -0.000017]
         [-0.000064  0.099765 -0.000101 -0.00002   0.000017]
         [ 0.000015 -0.000101  0.158635  0.000101  0.000015]
         [-0.000017 -0.00002   0.000101  0.168192  0.000064]
         [-0.000017  0.000017  0.000015  0.000064  0.169596]]
        """
        k_mat = self.kinetic_energy_matrix()
        j_mat = self.rotational_energy_matrix()
        v_mat = self.potential_energy_matrix()
        h_mat = k_mat + j_mat + v_mat
        return h_mat

    def wavenumbers(self):
        """Calculates the wavenumbers of the corresponding
        ro-vibrational energy levels.

        Returns
        -------
        w_vec : ndarray

        Notes
        -----
        The available energy states are the eigenvalues of the
        corresponding Hamiltonian matrix.

        Examples
        --------
        >>> import diatomdvr.dvr as dvr
        >>> c = [0.16969615, -3.66343563, 19.77175143]
        >>> a = 1.012540518
        >>> HCl = dvr.DVR(5, 1.7, 10, ['H', 'Cl'], c, a=a)
        >>> w_vec = HCl.wavenumbers()
        >>> for w in w_vec:
        ...     print(round(w, 4))
        21895.9248
        34816.2361
        36913.4045
        37222.7108
        38303.2094
        >>> HCl = dvr.DVR(500, 1.7, 10, ['H', 'Cl'], c, a=a)
        >>> w_vec = HCl.wavenumbers()
        >>> for w in w_vec[:5]:
        ...     print(round(w, 4))
        1476.551
        4339.9064
        7083.6022
        9707.6379
        12212.0144
        >>> print(round(w_vec[1] - w_vec[0], 4))
        2863.3554
        """
        h_mat = self.hamiltonian_matrix()
        # Eigenvalue vector of the Hamiltonian matrix. Ro-vibrational
        # energy levels in Hartree energy units.
        e_vec = np.linalg.eigvals(h_mat)
        e_vec.sort()
        # Converting the ro-vibrational energy levels to wavenumbers.
        w_vec = e_vec*0.01*DVR.e_h/(DVR.h*DVR.c)
        return w_vec
