"""Calculates the coefficients of a polynomial from the parameters of
a Morse potential.
"""


import numpy as np


def morse_to_poly(r_e, d, beta):
    r"""Calculates the fitting coefficients of a 2\ :sup:`nd` degree
    polynomial from the parameters of a Morse potential.

    Parameters
    ----------
    r_e : int or float
        Equilibrium bond length in Bohr units.
    d : int or float
        Well depth in Hartree energy units.
    beta : int or float
        Beta parameter.

    Returns
    -------
    a : float
        Constant parameter in the exponentials.
    c_vec : ndarray
        Coefficients of the polynomial.

    Notes
    -----
    The Morse potential of a diatomic molecule can be written as:

    .. math::
        V(r)=D\left[1-e^{-\frac{2\beta (r-r_e)}{r_e}}\right]^2,

    where :math:`D` is the well depth and :math:`r_e` is the
    equilibrium bond length. The potential can be rewritten in a
    polynomial form.

    .. math::
        V(r)
        =D
        -2De^{2\beta}e^{-\frac{2\beta r}{r_e}}
        +De^{4\beta}e^{-\frac{4\beta r}{r_e}}
        =c_0+c_1e^{-\frac{r}{a}}+c_2e^{-2\frac{r}{a}}

    The corresponding coefficients are :math:`c_0=D`,
    :math:`c_1=-2De^{2\beta}`, :math:`c_2=De^{4\beta}` and the
    parameter in the exponentials is :math:`a=\frac{r_e}{2\beta}`.

    Examples
    --------
    >>> import diatomdvr.morse_to_polynomial as morse_to_polynomial
    >>> r_e = 2.4088
    >>> d = 0.169696
    >>> beta = 1.1895
    >>> a, c_vec = morse_to_polynomial.morse_to_poly(r_e, d, beta)
    >>> c_vec.round(5)
    array([ 0.1697 , -3.66343, 19.77173])
    >>> round(a, 5)
    1.01253
    """
    if r_e <= 0:
        raise ValueError("The bond length should be positive.")
    if d <= 0:
        raise ValueError("The well depth should be positive.")
    if beta <= 0:
        raise ValueError("Parameter beta should be positive.")
    c_vec = np.array([d, -2*d*np.e**(2*beta), d*np.e**(4*beta)])
    a = r_e/(2*beta)
    return a, c_vec
