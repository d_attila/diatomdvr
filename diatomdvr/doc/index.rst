.. toctree::
    :maxdepth: 2
    :caption: Contents:

=========
diatomdvr
=========

diatomdvr reference
===================

diatomdvr.dvr
-------------

.. autoclass:: diatomdvr.dvr.DVR

.. raw:: latex

    \newpage

.. autofunction:: diatomdvr.dvr.DVR.potential_energy_matrix

.. autofunction:: diatomdvr.dvr.DVR.rotational_energy_matrix

.. autofunction:: diatomdvr.dvr.DVR.kinetic_energy_matrix

.. raw:: latex

    \newpage

.. autofunction:: diatomdvr.dvr.DVR.hamiltonian_matrix

.. raw:: latex

    \newpage

.. autofunction:: diatomdvr.dvr.DVR.wavenumbers

.. raw:: latex

    \newpage

diatomdvr.curve
---------------

.. autofunction:: diatomdvr.curve.fit

.. raw:: latex

    \newpage

.. autofunction:: diatomdvr.curve.potential_energy

.. autofunction:: diatomdvr.curve.optimize

.. raw:: latex

    \newpage

diatomdvr.morse_to_polynomial
-----------------------------

.. autofunction:: diatomdvr.morse_to_polynomial.morse_to_poly
