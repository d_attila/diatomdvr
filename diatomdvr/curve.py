"""Functions for working with potential energy curves of diatomic
molecules.
"""


import numpy as np


def fit(r_vec, v_vec, k, a=2):
    r"""Calculates the fitting coefficients of the `k`-th order
    polynomial of Morse variables using a least-squares method.

    Parameters
    ----------
    r_vec : array_like of int or float
        An array, list or tuple of bond lengths.
    v_vec : array_like of int or float
        An array, list or tuple of of potential energies.
    k : int
        The order of the fitted polynomial.
    a : int or float, optional
        A constant parameter in the exponential terms.

    Returns
    -------
    c_vec : ndarray

    Notes
    -----
    The potential energy curve of a diatomic molecule can be
    approximated with the following polynomial:

    .. math::
        V(r)
        =\sum\limits_{n=0}^{k}
          c_ne^{-n\frac{r}{a}}
        =\sum\limits_{n=0}^{k}
          c_ny^n,

    where :math:`y=e^{-\frac{r}{a}}` denotes a Morse variable.

    Examples
    --------
    >>> import diatomdvr.curve as curve
    >>> r_vec = [1.70000, 3.88083, 5.85000, 7.81917, 10.00000]
    >>> v_vec = [0.17443, 0.09965, 0.15854, 0.16808, 0.169508]
    >>> a = 1.01253
    >>> c_vec = curve.fit(r_vec, v_vec, k=2, a=a)
    >>> c_vec.round(5)
    array([ 0.1697 , -3.66365, 19.77313])
    """
    if min(r_vec) <= 0:
        raise ValueError("A bond length should be positive.")
    if not isinstance(k, int):
        raise TypeError("The order of the polynomial should be an int.")
    if k < 0:
        raise ValueError("The order of the fitting should be non-negative.")
    if a <= 0:
        raise ValueError("Parameter a should be positive.")
    n = len(r_vec)
    r_vec = np.array(r_vec)
    v_vec = np.array(v_vec)
    y_vec = np.e**(-r_vec/a)
    x_mat = np.array([[y_vec[i]**j for j in range(k + 1)]
                      for i in range(n)])
    p_mat = np.linalg.pinv(x_mat.T @ x_mat)
    c_vec = p_mat @ x_mat.T @ v_vec
    return c_vec


def potential_energy(r, c_vec, a=2):
    """Calculates the potential energy at a given bond length.

    Parameters
    ----------
    r : int or float
        Bond length.
    c_vec : array_like of int or float
        A vector of the coefficients of the fitting polynomial of
        Morse variables.
    a : int or float, optional
        A constant parameter in the exponential terms.

    Returns
    -------
    v : float

    Examples
    --------
    >>> import diatomdvr.curve as curve
    >>> r = 1.7
    >>> c_vec = [0.16970, -3.66365, 19.77313]
    >>> a = 1.01253
    >>> v = curve.potential_energy(r, c_vec, a=a)
    >>> round(v, 5)
    0.17443
    """
    if r <= 0:
        raise ValueError("The bond length should be positive.")
    if a <= 0:
        raise ValueError("Parameter a should be positive.")
    n = len(c_vec)
    v = sum([c_vec[i]*np.e**(-i*r/a) for i in range(n)])
    return v


def optimize(c_vec, x=1, a=2, iterations=1000, pz=10):
    """Calculates the equilibrium bond length.

    Parameters
    ----------
    c_vec : array_like of int or float
        A vector of the coefficients of the fitting polynomial of
        Morse variables.
    x : int or float, optional
        Initial guess of the equilibrium bond length.
    a : int or float, optional
        A constant parameter in the exponential terms.
    iterations : int, optional
        The maximum number of iterations of the Newton-Raphson
        algorithm.
    pz : int, optional
        Setting the decimal precision.

    Returns
    -------
    r : float

    Examples
    --------
    >>> import diatomdvr.curve as curve
    >>> c_vec = [0.16970, -3.66365, 19.77313]
    >>> a = 1.01253
    >>> r = curve.optimize(c_vec, a=a)
    >>> round(r, 5)
    2.40882
    """
    if a <= 0:
        raise ValueError("Parameter a should be positive.")
    n = len(c_vec)
    for i in range(iterations):
        fx = sum([-j*c_vec[j]/a*np.e**(-j*x/a) for j in range(n)])
        dx = sum([j**2*c_vec[j]/a**2*np.e**(-j*x/a) for j in range(n)])
        temp = x
        x = x - fx/dx
        if round(abs(x - temp), pz) == 0:
            break
        if i == iterations - 1:
            raise Exception("Newton-Raphson did not converge.")
    r = x
    return r
